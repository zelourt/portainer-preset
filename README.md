## Install by script
```bash
$ curl -s -L https://gitlab.com/zelourt/portainer-preset/-/raw/main/script.sh | bash
```

## Setup portainer

```bash
$ docker compose up --build -d
```
